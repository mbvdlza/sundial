'use strict';

/**
 * Error and Status Codes.
 * This should be treated as constants to minimise (nay, eliminate) magic numbers in the code base.
 * @type {{Json}}
 */
var statusCodes = {
    error: {
        NOT_FOUND: { code: 404,     message: 'Not Found'},
        BAD_REQUEST: {code: 400,    message: 'Bad Request'},
        SERVER_ERROR: {code: 500,   message: 'Internal Server Error'}
    },
    success: {
        OK: { code: 200,            message: 'OK'}
    }
};

module.exports = statusCodes;
