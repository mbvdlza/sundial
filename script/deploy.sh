#!/bin/bash

ssh m@m.notnull.xyz <<EOF
  cd ~/node/sundial
  git pull
  npm install --production
  export NODE_ENV=production
  pm2 reload all
  exit
EOF

