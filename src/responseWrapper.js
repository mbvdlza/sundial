/**
 * This module will handle the wrapping of responses into their proper formats before
 * being returned in a response to the caller.
 *
 * @todo: the status codes and exact data format is to be determined, but data:"" will depend on
 * the requested method in the end anyway.
 */

/**
 * responseStructure:
 * data: the response from the api method
 * timestamp: current unix epoch
 * status: Using the same status codes as http, for clarity. 200 = OK. Use statusCodes.js
 */

'use strict';

var util = require('./util');
var responseStructure;

var $ = {

    buildResponse: function(res, data, code, callback) {
        responseStructure = {
            data: data,
            timestamp: Math.floor(new Date() / 1000),
            status: code,
            version: util.getApiVersion()
        };
        callback(res, responseStructure, code);
    }

};

module.exports = $;
