'use strict';

var chance = require('chance').Chance();
var logger = require('../logger');
var statusCodes = require('../statusCodes');

/**
 * Generates a completely befuzzling random string.
 * @returns {String}
 */
var randomString = function() {
    return chance.string();
};

/**
 * Generates a random address using some Chance magic.
 * @returns {String}
 */
var randomAddress = function() {
    return chance.address();
};

/**
 * Generates a random integer between min and max
 * @param args [optional] which is a list of two values, min and max.
 * @returns {Number}
 */
var randomInteger = function(args) {
    var minR = args[0] ? args[0] : Number.MIN_SAFE_INTEGER;
    var maxR = args[1] ? args[1] : Number.MAX_SAFE_INTEGER;

    // If anyone tries to be a wise-ass, we flip the values
    if (minR > maxR) {
        logger.debug('randomInteger() : Wise ass correction. Min/Max flip.');
        var temp = maxR;
        maxR = minR;
        minR = temp;
    }

    return chance.integer({min: minR, max: maxR});
};

/**
 * Return a random somewhat-pronouncable word.
 * Optionally, provide a number of 'syllables' to generate.
 * @param args [optional, default 3] list with a int value, representing syllables.
 */
var randomWord = function(args) {
    var syls = args[0] ? args[0] : 3;
    if (syls === 0) {
        syls = 1;
    } else if (syls > 10) {
        syls = 10;
    }

    return chance.word({syllables: syls});
};

/**
 * Generate a random firstname and lastname, with an optional prefix or gender.
 * @param args [Optional] list taking two values. prefix:boolean, gender:male/female
 */
var randomName = function(args) {
    var prefix = (args[0] === true) ? args[0] : false;
    var gender = (args[1] === 'female') ? args[1] : 'male';
    return chance.name({ prefix: prefix, gender: gender });
};

/**
 * Generate a random color in a web friendly Hex value or RGB value.
 * @param args [Optional] List containing a single value, 'hex' or 'rgb'. Default: 'hex'
 */
var randomColor = function(args) {
    var type = (args[0] === 'rgb') ? args[0] : 'hex';
    return chance.color({format: type});
};

/**
 * Generate a random web address (for now, email or domain name).
 * @param args [Optional] List containing one element, either 'url' or 'email'(default)
 */
var randomWeb = function(args) {
    var resp;
    if (args[0] === 'url') {
        resp = chance.domain();
    } else {
        resp = chance.email();
    }

    return resp;
};

var $ = {};

/**
 * Handle the option value to decide which chance method/s to call.
 *
 * @param option A string noting the option.
 * @param args a list of values that pertains to the method called.
 * @param callback
 */
$.handleChanceOption = function(option, args, callback) {
    logger.debug('Random : Handling Chance Option %s', option);

    var err = null;
    var randomResponse = statusCodes.error.BAD_REQUEST.message + ': Unsupported Option';
    switch (option) {
        case 'string':
            randomResponse = randomString();
            break;
        case 'address':
            randomResponse = randomAddress();
            break;
        case 'int':
            randomResponse = randomInteger(args);
            break;
        case 'word':
            randomResponse = randomWord(args);
            break;
        case 'name':
            randomResponse = randomName(args);
            break;
        case 'color':
            randomResponse = randomColor(args);
            break;
        case 'web':
            randomResponse = randomWeb(args);
            break;

        /** Default is to bail with a Status 400 and Unsupported Option type message **/
        default:
            err = statusCodes.error.BAD_REQUEST.code;
            break;
    }

    callback(err, randomResponse);
};

module.exports = $;
