/**
 * Utilities/Tools
 */

'use strict';

var packageJson = require('../package.json');

var $ = {};

/**
 * Convert a given unix timestamp to a human readable form like 21:11:09
 * @param timestamp unix timestamp
 * @returns {string}
 */
$.timeUnixToHuman = function(timestamp) {
    var date = new Date(timestamp * 1000); // turn seconds to milliseconds
    var hours = '0' + date.getHours();
    var minutes = '0' + date.getMinutes();
    var seconds = '0' + date.getSeconds();
    var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
    return formattedTime;
};

/**
 * Normalize a port into a number, string, or false.
 */
$.normalizePort = function(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
};

/**
 * Read the version from package.json and return that.
 * Will return 0.0.0 when it can not be resolved from the packageJson object.
 * @returns string
 */
$.getApiVersion = function() {
    return packageJson.version || '0.0.0';
};

module.exports = $;
