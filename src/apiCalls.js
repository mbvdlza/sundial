/**
 * apiCalls to external services/api's.
 * Returns json (manipulated most of the time) in all cases.
 */

'use strict';

var config = require('../config.json');
var http = require('http');
var wrapper = require('./responseWrapper');
var logger = require('../logger');
var chance = require('./chance');
var util = require('./util');
var statusCodes = require('../statusCodes');

// This pertains to the openweathermap api we use. See config.
var weatherConfig = config.secrets.filter(function(v) {
    return v.provider === 'weather';
});

var urlPathHead = '/data/2.5/';
var urlPathTail = '?APPID=' + weatherConfig[0].apikey + '&units=metric&q=';
var optionsWeather = {
    host: weatherConfig[0].url,
    port: 80,
    path: '',
    method: 'POST'
};

function sendResponse(res, data, statusCode) {
    res.status(statusCode).send(data);
}

var apiCalls = {

    getWeatherByCityName: function(req, routerResponse, city) {
        optionsWeather.path = urlPathHead + 'weather' + urlPathTail + city;
        http.get(optionsWeather, function(apires) {
            logger.info('req getWeatherByCityName for %s', city);
            apires.setEncoding('utf8');
            apires.on('data', function(chunk) {

                try {
                    var cleanChunk = JSON.parse(chunk);

                    if (cleanChunk.cod === '404') {
                        wrapper.buildResponse(routerResponse, 'City Not Found',
                            statusCodes.error.NOT_FOUND.code, sendResponse);
                        logger.info('failed getWeatherByCityName for %s cod: %d', city, cleanChunk.cod);
                    } else {
                        // when successful, these less-useful nodes can be dropped.
                        delete cleanChunk.coord;
                        delete cleanChunk.base;
                        delete cleanChunk.cod;

                        wrapper.buildResponse(routerResponse, cleanChunk, statusCodes.success.OK.code, sendResponse);
                    }
                } catch (e) {
                    wrapper.buildResponse(routerResponse, 'now chunk parsing failure',
                        statusCodes.error.SERVER_ERROR.code, sendResponse);
                    logger.info('failed getWeatherByCityName for %s - exception %s', city, e);
                }

            });
        }).on('error', function(e) {
            logger.error('getWeatherByCityName error : %s', e.message);
            routerResponse.render('error', {
                message: e.message,
                error: e
            });
        }).end();
    },

    getForecastByCityName: function(routerResponse, city) {
        optionsWeather.path = urlPathHead + 'forecast/daily' + urlPathTail + city + '&cnt=5';

        http.get(optionsWeather, function(apires) {
            logger.info('getForecastByCityName for %s', city);
            apires.setEncoding('utf8');
            var content = '';

            apires.on('data', function(chunk) {
                content += chunk;
            });

            apires.on('end', function() {
                try {
                    var parsed = JSON.parse(content);

                    delete parsed.city.id;
                    delete parsed.city.population;
                    delete parsed.city.coord;
                    delete parsed.cod;
                    delete parsed.message;

                    for (var x in parsed.list) {
                        if (parsed.list.hasOwnProperty(x)) {
                            delete parsed.list[x].weather.id;
                            delete parsed.list[x].weather.icon;
                            parsed.list[x].date = new Date(parsed.list[x].dt * 1000).toISOString();
                            delete parsed.list[x].dt;

                            parsed.list[x].windspeed = parsed.list[x].speed;
                            delete parsed.list[x].speed;

                            parsed.list[x].winddirection = parsed.list[x].deg;
                            delete parsed.list[x].deg;
                        }
                    }

                    parsed.days = parsed.list;
                    delete parsed.list;

                    wrapper.buildResponse(routerResponse, parsed, statusCodes.success.OK.code, sendResponse);
                } catch (e) {
                    wrapper.buildResponse(routerResponse, 'forecast chunk parsing failure',
                        statusCodes.error.SERVER_ERROR.code, sendResponse);
                    logger.info('failed getWeatherByCityName for %s - exception %s', city, e);
                }
            });
        });
    },

    /**
     * Responds with some status information. Mostly used by uptime monitors.
     * @param routerResponse
     */
    getStatus: function(routerResponse) {
        var statusData = {};

        statusData.platform = process.platform;
        statusData.up = process.uptime();
        statusData.apiVersion = util.getApiVersion();
        statusData.potatoes = process.version + '_' + process.pid + '_' + process.env.USER;

        wrapper.buildResponse(routerResponse, statusData, statusCodes.success.OK.code, sendResponse);
    },

    /**
     * Handle the apicalls to /random/ which is a POST request.
     * @param routerResponse
     * @param body containing (string)'option' and sometimes (array)'args'
     */
    getRandomOption: function(routerResponse, body) {

        var option = -1;
        var args = [];

        if (body === undefined) {
            logger.error('getRandomOption() called without request body');
        } else {
            if (body.option !== undefined) {
                option = body.option;
            } else {
                logger.error('getRandomOption() data requires a \'option\' property');
            }

            if (body.args !== undefined) {
                if (Array.isArray(body.args)) {
                    args = body.args;
                } else {
                    logger.debug('getRandomOption() : Passed arguments are not in an array. Ignoring.');
                }
            }
        }

        chance.handleChanceOption(option, args, function(err, data) {
            var code;
            if (err) {
                data = statusCodes.error.BAD_REQUEST.message + ': Unsupported Option';
                code = statusCodes.error.BAD_REQUEST.code;
            } else {
                code = statusCodes.success.OK.code;
            }

            wrapper.buildResponse(routerResponse, data, code, sendResponse);
        });
    }
};

module.exports = apiCalls;
