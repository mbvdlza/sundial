'use strict';

/**
 * Database connection for Redis.
 * @author Marlon van der Linde <marlon@kbye.co.za>
 */

var redis = require('redis');
var config = require('../config.json');
var logger = require('../logger');
var bluebird = require('bluebird');
var client;

var options = {
    host: config.db.host || '127.0.0.1',
    port: config.db.port || 6379,
    max_attempts: 10,
    connect_timeout: 900000,  // 900000 ms (15 mins)
    retry_max_delay: 5000
};

module.exports = function(callback) {
    client = redis.createClient(options);

    //client.auth(config.db.password,...)
    client = bluebird.promisifyAll(client);

    client.on('ready', function() {
        logger.info('redis state -- ready');
        callback(null, client);

    }).on('reconnecting', function(attempt, delay) {
        logger.info('redis state -- reconnect: Attempt %s / Delay %s', attempt, delay);
    }).on('error', function(err) {
        callback(err);
    }).on('idle', function() {
        logger.debug('redis state -- idle');
    }).on('end', function() {
        logger.info('redis state -- end');
    });

};
