#!/usr/bin/env node

'use strict';

var logger = require('../logger');
var util = require('../src/util');

var db;

var expressStartup = function() {

    /**
     * Event listener for HTTP server "error" event.
     */

    function onError(error) {
        if (error.syscall !== 'listen') {
            throw error;
        }

        var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

        // handle specific listen errors with friendly messages
        switch (error.code) {
            case 'EACCES':
                console.error(bind + ' requires elevated privileges');
                process.exit(1);
                break;
            case 'EADDRINUSE':
                console.error(bind + ' is already in use');
                process.exit(1);
                break;
            default:
                throw error;
        }
    }

    /**
     * Event listener for HTTP server "listening" event.
     */
    function onListening() {
        var addr = server.address();
        var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
        debug('Listening on ' + bind);
    }

    /**
     * Module dependencies.
     */

    var app = require('../sundial');
    var debug = require('debug')('sundial:server');
    var http = require('http');

    /**
     * Get port from environment and store in Express.
     */

    var port = util.normalizePort(process.env.PORT || '3000');
    app.set('port', port);

    /**
     * Create HTTP server.
     */

    var server = http.createServer(app);

    /**
     * Listen on provided port, on all network interfaces.
     */

    server.listen(port);
    server.on('error', onError);
    server.on('listening', onListening);
};

/**
 * Wait for the database ready promise before resuming the express ignition.
 */
require('../src/db')(function(err, redisClient) {
    if (err) {
        logger.error('Cannot Continue: %s', err);
        process.exit();
    }

    db = redisClient;
    logger.debug('DB called back! Ready: %s', redisClient.ready);
    expressStartup();
});
