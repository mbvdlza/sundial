'use strict';

var express = require('express');
var router = express.Router();

/**
 * Help routes
 */
router.get('/', function(req, res) {
    res.render('help', { title: 'Sundial Help' });
});

module.exports = router;
