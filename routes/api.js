/**
 * Router for
 */

'use strict';

var express = require('express');
var router = express.Router();
var apiCalls = require('../src/apiCalls');
var logger = require('../logger');

// as an api, we'll handle info at /help and don't need anything at /
router.get('/', function(req, res) {
    logger.info('GET /');
    res.redirect('/help');
});

/**
 * GET
 */
router.get('/weather/now/:city', function(req, res) {
    logger.info('GET /weather/now/%s', req.params.city);
    apiCalls.getWeatherByCityName(req, res, req.params.city);
});

router.get('/weather/forecast/:city', function(req, res) {
    logger.info('GET /weather/forecast/%s', req.params.city);
    apiCalls.getForecastByCityName(res, req.params.city);
});

router.get('/status', function(req, res) {
    logger.info('GET /status');
    apiCalls.getStatus(res);
});

/**
 * POST
 */
router.post('/random', function(req, res) {
    logger.info('POST /random');
    apiCalls.getRandomOption(res, req.body);
});

module.exports = router;
