'use strict';

var winston = require('winston');
var config = require('./config.json');
var fs = require('fs');

/**
 * Logging is not possible without the log dir, creating it synchronously before attempting to write to it.
 */
if (!fs.existsSync(config.logging.dir)) {
    console.log('Log output directory ' + config.logging.dir + ' doesn\'t exist. Creating.');
    fs.mkdirSync(config.logging.dir);
}

var logger = new (winston.Logger)({
    transports: [
        new (winston.transports.File)({
            name: 'info.log',
            filename: config.logging.dir + config.logging.loginfo,
            level: 'info',
            handleExceptions: true,
            json: false,
            maxsize: 5242880, //5MB
            maxFiles: 5,
            colorize: false
        }),
        new (winston.transports.File)({
            name: 'error.log',
            filename: config.logging.dir + config.logging.logerror,
            level: 'error',
            handleExceptions: true,
            json: false,
            maxsize: 5242880, //5MB
            maxFiles: 5,
            colorize: false
        }),
        new winston.transports.Console({
            handleExceptions: true,
            colorize: true,
            json: false,
            level: 'debug'
        })
    ]
});

module.exports = logger;

