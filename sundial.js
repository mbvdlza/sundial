/**
 * Sundial
 */

'use strict';

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routesHelp = require('./routes/help');
var routesApi = require('./routes/api');

var statusCodes = require('./statusCodes');
var logger = require('./logger');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routesApi);
app.use('/help', routesHelp);

app.use(function(req, res) {
    res.status(404);
    logger.info('404 Handled : %s', req.url);

    if (req.accepts('html')) {
        res.render('404',
            { url: req.url, code: statusCodes.error.NOT_FOUND.code, message: statusCodes.error.NOT_FOUND.message });
        return;
    }

    if (req.accepts('json')) {
        res.send({ error: req.url + ' ' + statusCodes.error.NOT_FOUND.message });
        return;
    }

    // default: text
    res.type('txt').send(req.url + ' ' + statusCodes.error.NOT_FOUND.message);
});

/**
 * Error Handlers !!
 */

/** development error handler **/
if (app.get('env') === 'development') {
    app.use(function(err, req, res) {
        res.status(err.status || statusCodes.error.SERVER_ERROR.code);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

/** production error handler **/
app.use(function(err, req, res) {
    res.status(err.status || statusCodes.error.SERVER_ERROR.code);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;
